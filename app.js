/**
 * @param {number[]} g
 * @param {number[]} s
 * @return {number}
 */
var findContentChildren = function(g, s) {
    g = g.sort();
    s = s.sort();
    let count = 0;
    for (let i = 0; i < s.length; i++) {
        for (let j = 0; j < g.length; j++) {
            if (g[j] <= s[i]) {
                g.splice(j, 1);
                s.splice(i, 1);
                i--;
                count++;
                break;
            }
        }
    }

    return count;
};